import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { StatusCodes } from "http-status-codes";
import { AxiosResponse } from "axios";

import * as jwt from 'jsonwebtoken';
import { PharmacyModels } from "../../models/service-models/pharmacy_datas-model";

export default async (fastify: FastifyInstance, _options: any, done: any) => {
    const serviceModels = new PharmacyModels();

    fastify.get('/:cid', {
        preHandler: [],
    },
        async (request: FastifyRequest, reply: FastifyReply) => {
            const req: any = request;
            const cid = req.params.cid;

            try {
                const hisSecretKey: any = `5a8d2d734a1352f8d113d22eabf2d1cb`;

                var token: any = jwt.sign({
                    exp: Math.floor(Date.now() / 1000) + (2 * 60), // 2 minute
                }, hisSecretKey);
                const response = await serviceModels.getImportPharmacy(token, {cid: cid});
                // console.log(response);
                const data: any = response.data;
                reply.status(StatusCodes.OK).send(data);
            } catch (error) {
                request.log.error(error);
                reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
            }
        })

    }