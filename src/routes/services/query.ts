import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { StatusCodes } from "http-status-codes";
import { AxiosResponse } from "axios";
import * as jwt from 'jsonwebtoken';
import { ServiceModels } from "../../models/service-models/smr_datas-model";
export default async (fastify: FastifyInstance, _options: any, done: any) => {
    const serviceModels = new ServiceModels();

    fastify.get('/hospital/:cid', {
        preHandler: [],
    },
        async (request: FastifyRequest, reply: FastifyReply) => {
            const req: any = request;
            const cid = req.params.cid;
            try {
                const hisSecretKey: any = process.env.API_HIS_SECRET_KEY || '';

                var token: any = jwt.sign({
                    exp: Math.floor(Date.now() / 1000) + (2 * 60), // 2 minute
                }, hisSecretKey);
                const response: AxiosResponse = await serviceModels.getHospitalByCid(fastify.axios, token, cid);
                const data: any = response.data;
                reply.status(StatusCodes.OK).send(data);
            } catch (error) {
                request.log.error(error);
                reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
            }
        })

    fastify.get('/service/:hospcode/:cid', {
        preHandler: [],
    },
        async (request: FastifyRequest, reply: FastifyReply) => {
            const req: any = request;
            const hospcode = req.params.hospcode;
            const cid = req.params.cid;
            try {
                const hisSecretKey: any = process.env.API_HIS_SECRET_KEY || '';

                var token: any = jwt.sign({
                    exp: Math.floor(Date.now() / 1000) + (2 * 60), // 2 minute
                }, hisSecretKey);
                const response: AxiosResponse = await serviceModels.getService(fastify.axios, token, hospcode,cid);
                const data: any = response.data.data;
                reply.status(StatusCodes.OK).send(data);
            } catch (error) {
                request.log.error(error);
                reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
            }
        })

        fastify.get('/service-profile/:hospcode/:hn/:seq', {
            preHandler: [],
        },
            async (request: FastifyRequest, reply: FastifyReply) => {
                const req: any = request;
                const hospcode = req.params.hospcode;
                const hn = req.params.hn;
                const seq = req.params.seq;
                try {
                    const hisSecretKey: any = process.env.API_HIS_SECRET_KEY || '';
    
                    var token: any = jwt.sign({
                        exp: Math.floor(Date.now() / 1000) + (2 * 60), // 2 minute
                    }, hisSecretKey);
                    const response: AxiosResponse = await serviceModels.getServiceProfile(fastify.axios, token, hospcode,hn,seq);
                    const data: any = response.data.data;
                    reply.status(StatusCodes.OK).send(data);
                } catch (error) {
                    request.log.error(error);
                    reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
                }
            }
        )
        fastify.post('/saveLogs', {
            preHandler: [],
        },
            async (request: FastifyRequest, reply: FastifyReply) => {
                const req: any = request;
                let info: any = req.body;
                info.hospcode = process.env.API_HIS_CODE;

                try {
                    const hisSecretKey: any = process.env.API_HIS_SECRET_KEY || '';
    
                    var token: any = jwt.sign({
                        exp: Math.floor(Date.now() / 1000) + (2 * 60), // 2 minute
                    }, hisSecretKey);
                    const response: AxiosResponse = await serviceModels.saveLogs(fastify.axios, token, info);
                    const data: any = response.data.data;
                    reply.status(StatusCodes.OK).send(data);
                } catch (error) {
                    request.log.error(error);
                    reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
                }
            }
        )
    done();
}