import { FastifyInstance } from "fastify";

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  // verify jwt token
  // fastify.addHook("onRequest", (request) => request.jwtVerify());

  fastify.register(require('./services'), { prefix: '/services' });
  fastify.register(require('./login'), { prefix: '/login' });
  fastify.register(require('./test'), { prefix: '/' });
  fastify.register(require('./query'), { prefix: '/query' });
  fastify.register(require('./pharmacy'), { prefix: '/pharmacy' });

  done();

} 
