import { Knex } from 'knex';
export class HisHosxpv3Model {
  async getLogin(db: Knex, username: any, password: any) {
    let data = await db.raw(`
    select o.loginname as username , 
           name as fullname, 
           (select hospitalcode from opdconfig limit 1) as hcode				
    from opduser o		 
    where  o.loginname = '${username}' 		
           and passweb = md5('${password}')
    `);
    return data[0];
  }
  async getVisit(db: Knex, startDate: any, endDtae: any) {
    let data = await db.raw(`
    SELECT
          o.vn as seq,
          o.hn as hn,
          p.pname as title_name,
          p.fname as first_name,
          p.lname as last_name,
          o.vstdate as date_serv,
          o.vsttime as time_serv,
          c.department as department
    FROM ovst as o
    INNER JOIN kskdepartment as c ON c.depcode = o.main_dep
    INNER JOIN patient as p ON p.hn = o.hn
    WHERE o.vstdate between '${startDate}' and '${endDtae}'
    UNION
    SELECT
          an.an as seq,
          an.hn as hn,
          p.pname as title_name,
          p.fname as first_name,
          p.lname as last_name,
          i.regdate as date_serv,
          i.regtime as time_serv,
          w.name as department
    FROM an_stat as an
    INNER JOIN ward as w ON w.ward = an.ward
    INNER JOIN patient as p ON p.hn = an.hn
    INNER JOIN ipt as i On i.an = an.an
    WHERE i.regdate between '${startDate}' and '${endDtae}'
    `);
    return data[0];
  } 
  async getServices(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`
    SELECT
          o.vn as seq,
          o.hn as hn,
          p.pname as title_name,
          p.fname as first_name,
          p.lname as last_name,
          DATE_FORMAT(date(o.vstdate),'%Y-%m-%d') as date_serv,
          DATE_FORMAT(time(o.vsttime),'%h:%i:%s') as time_serv,
          c.department as department
    FROM ovst as o
    INNER JOIN kskdepartment as c ON c.depcode = o.main_dep
    INNER JOIN patient as p	ON p.hn = o.hn
    WHERE o.hn ='${hn}' and o.vn = '${seq}' 
    UNION
    SELECT
          an.an as seq,
          an.hn as hn,
          p.pname as title_name,
          p.fname as first_name,
          p.lname as last_name,
          DATE_FORMAT(date(i.regdate),'%Y-%m-%d') as date_serv,
          DATE_FORMAT(time(i.regtime),'%h:%i:%s') as time_serv,
          w.name as department
    FROM an_stat as an
    INNER JOIN ward as w ON w.ward = an.ward
    INNER JOIN patient as p ON p.hn = an.hn
    INNER JOIN ipt as i On i.an = an.an
    WHERE an.hn ='${hn}' and an.an = '${seq}'
    `);
    return data[0];
  }
  async getProfile(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`
    select p.hn as hn, 
    p.cid as cid, 
    p.pname as title_name,
    p.fname as first_name,
    p.lname as last_name,
    s.name as sex,
    oc.name as occupation,
    IF((p.moopart ="" or p.moopart='-'  or p.moopart is null), "00", IF(CHAR_LENGTH(p.moopart ) < 2, LPAD(p.moopart ,2,"0"),p.moopart ) ) AS moopart,
    p.addrpart,
    p.tmbpart,
    p.amppart,
    p.chwpart,
    p.birthday as brthdate,
    concat(LPAD(age_y,3,0),'-',LPAD(age_m,2,0),'-',LPAD(age_d,2,0)) as age,
    o.pttype as pttype_id,
    t.name as pttype_name,
    o.pttypeno as pttype_no,
    o.hospmain,
    c.name as hospmain_name,
    o.hospsub,
    h.name as hospsub_name,
    p.firstday as registdate,
    p.last_visit as visitdate,
    p.fathername as father_name,
    p.mathername as mother_name,
    p.informrelation as contact_name,
    p.informtel as contact_mobile,
    oc.name as occupation
  FROM ovst o 
  INNER JOIN patient as p on p.hn=o.hn
  INNER JOIN vn_stat as vn on vn.vn=o.vn 
  LEFT OUTER JOIN pttype as t on o.pttype=t.pttype
  LEFT OUTER JOIN hospcode as c on o.hospmain=c.hospcode
  LEFT OUTER JOIN hospcode as h on o.hospsub=h.hospcode
  LEFT OUTER JOIN sex s on s.code = p.sex 
  LEFT OUTER JOIN occupation oc on oc.occupation = p.occupation
  WHERE p.hn = '${hn}' ORDER BY o.vstdate DESC
  LIMIT 1
    `);
    return data[0];
  }
  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`
      SELECT hospitalcode as provider_code,
             hospitalname as provider_name 
      from opdconfig limit 1
     `);
    return data[0];
  }
  async getAllergyDetail(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT agent as drug_name, 
           symptom as symptom ,
           begin_date as begin_date,
           date(entry_datetime) as daterecord
    FROM opd_allergy
    WHERE hn ='${hn}'
    `);
    return data[0];
  }
  async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT o.vn as seq,
        date_format(o.vstdate,'%Y-%m-%d') as date_serv,
        time(o.vsttime) as time_serv,
      if((d.icd10="" or d.icd10 is null),(select ro.pdx from referout ro where ro.vn=o.vn and d.vn ='${seq}' limit 1),d.icd10) as icd_code,
         REPLACE(i.name,"\r\n"," ") as icd_name,
        d.diagtype as diag_type, 
    if((dt.provisional_dx_text="" or dt.provisional_dx_text is null),(select ro.pre_diagnosis from referout ro where ro.vn=o.vn and d.vn ='${seq}' limit 1) ,dt.provisional_dx_text) as DiagNote,     
        '' as diagtype_id          
    FROM ovst as o
    INNER JOIN vn_stat v on v.vn=o.vn
    INNER JOIN ovstdiag d on d.vn = o.vn
    INNER JOIN icd101 as i on i.code = d.icd10
    INNER JOIN referout as ro on ro.vn=o.vn
    left JOIN ovstdiag_text dt on o.vn = dt.vn 
    WHERE d.vn ='${seq}' 
    and (o.an = '' or o.an is null)
    UNION
    SELECT  i.an as seq, 
        date_format(i.dchdate,'%Y-%m-%d') as date_serv, 
        time(i.dchtime) as time_serv,
      r.icd10 as icd_code,
      REPLACE(c.name,"\r\n"," ") as icd_name,
        r.diagtype as diag_type, 
        c.name as DiagNote,           
        r.diagtype as diagtype_id          
    FROM ipt i
    INNER JOIN (
    select  an,icd10,diagtype,'ipdx' as f from iptdiag id  where id.an = '${seq}'
    union 
    select vn as an,pdx as icd10,'1' as diagtype,'referdx' as f from referout ro where ro.vn='${seq}'
    ) as r on r.an=i.an
    inner join icd101 as c on r.icd10 = c.code
    where if((select count(an) as total from (
    select  an,icd10,diagtype,'ipdx' as f from iptdiag id  where id.an = '${seq}'
    union 
    select vn as an,pdx as icd10,'1' as diagtype,'referdx' as f from referout ro where ro.vn='${seq}'
    )  as r )> 1 , r.f='ipdx' ,r.f='referdx')
    `);
    return data[0];
  }
  async getDrugs(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT a.seq,a.date_serv,a.time_serv,REPLACE(a.drug_name,"\r\n"," ") as drug_name,a.qty,a.unit,REPLACE(a.usage_line1,"\r\n"," ") as usage_line1,REPLACE(a.usage_line2,"\r\n"," ") as usage_line2,REPLACE(a.usage_line3,"\r\n"," ") as usage_line3
    FROM (
    select o.vn as seq,
           DATE_FORMAT(date(i.rxdate),'%Y%m%d') as date_serv,
           DATE_FORMAT(time(i.rxtime),'%h:%i:%s') as time_serv,
           d.name as drug_name,
           (i.qty) as qty,
           d.units as unit ,
           if((select name1 from drugusage g where g.drugusage = i.drugusage) !='', (select name1 from drugusage g where g.drugusage = i.drugusage), (select s.name1 from sp_use s where s.sp_use = i.sp_use))  as usage_line1,
           if((select name2 from drugusage g where g.drugusage = i.drugusage) !='', (select name2 from drugusage g where g.drugusage = i.drugusage), (select s.name2 from sp_use s where s.sp_use = i.sp_use))  as usage_line2,
           if((select name3 from drugusage g where g.drugusage = i.drugusage) !='', (select name3 from drugusage g where g.drugusage = i.drugusage), (select s.name3 from sp_use s where s.sp_use = i.sp_use))  as usage_line3
    FROM ovst o
    left outer join opitemrece as i on (o.vn = i.vn)
    left outer Join drugitems as d ON i.icode = d.icode
    left outer join income as ic on i.income = ic.income
    WHERE (o.an is null or o.an = "" ) and i.icode in(select icode from drugitems) and ((i.income in ("03","04")) or (ic.group1 = "3")) and i.vn = '${seq}'
    union
    select o.vn as seq,
           DATE_FORMAT(date(i.rxdate),'%Y%m%d') as date_serv,
           DATE_FORMAT(time(i.rxtime),'%h:%i:%s') as time_serv,
           d.name as drug_name,
           (i.qty) as qty,
           d.units as unit ,
           if((select name1 from drugusage g where g.drugusage = i.drugusage) !='', (select name1 from drugusage g where g.drugusage = i.drugusage), (select s.name1 from sp_use s where s.sp_use = i.sp_use))  as usage_line1,
           if((select name2 from drugusage g where g.drugusage = i.drugusage) !='', (select name2 from drugusage g where g.drugusage = i.drugusage), (select s.name2 from sp_use s where s.sp_use = i.sp_use))  as usage_line2,
           if((select name3 from drugusage g where g.drugusage = i.drugusage) !='', (select name3 from drugusage g where g.drugusage = i.drugusage), (select s.name3 from sp_use s where s.sp_use = i.sp_use))  as usage_line3
    FROM ovst o
    left outer join opitemrece as i on (o.an = i.an)
    left outer Join drugitems as d ON i.icode = d.icode
    left outer join income as ic on i.income = ic.income 
    WHERE (i.an is not null and i.vn is null ) and i.icode in(select icode from drugitems)  and ((i.income in ("03","04")) or (ic.group1 = "3")) and i.an = '${seq}'
    ) a
    WHERE a.seq is not NULL
    `);
    return data[0];
  }
  async getLabs(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    select receive_date as 'date_serv',
    receive_time as 'time_serv',
    form_name as 'labgroup',
    lab_items_name_ref as 'lab_name',
    REPLACE(lab_order_result,"\r\n"," ") as lab_result,
    li.lab_items_unit as 'unit',
    if(lo.lab_items_normal_value_ref ="",if((li.lab_items_normal_value is null or li.lab_items_normal_value=''),"is null",li.lab_items_normal_value),lo.lab_items_normal_value_ref) as standard_result
    from ovst o
    inner join lab_head lh on o.vn = lh.vn or o.an = lh.vn
    inner join lab_order lo on lh.lab_order_number = lo.lab_order_number
    inner join lab_items li on li.lab_items_code = lo.lab_items_code
    where lh.vn = '${seq}' and lo.lab_order_result is not NULL
    `);
    return data[0];
  }
  async getProcedure(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT o.hn as pid,
    o.vn as seq,
    o.vstdate as date_serv,	
    o.vsttime as time_serv,
    od.icd10 as procedure_code,
    ic.name as procedure_name,
    DATE_FORMAT(date(od.vstdate),'%Y%m%d') as start_date,	
    DATE_FORMAT(time(od.vsttime),'%h:%i:%s') as start_time,
    DATE_FORMAT(date(od.vstdate),'%Y%m%d') as end_date,
    '00:00:00' as end_time
    FROM ovst o
    LEFT OUTER JOIN ovstdiag od on od.vn = o.vn
    INNER JOIN icd9cm1 ic on ic.code = od.icd10 
    WHERE o.vn = '${seq}'
    GROUP BY o.vn,od.icd10
    UNION
    SELECT o.hn as pid,
        o.vn as seq,
        o.vstdate as date_serv,	
        o.vsttime as time_serv,
        i.icd9 as procedure_code,
        c.name as procedure_name,
        DATE_FORMAT(date(i.opdate),'%Y%m%d') as start_date,	
        DATE_FORMAT(time(i.optime),'%h:%i:%s') as start_time,
        DATE_FORMAT(date(i.enddate),'%Y%m%d') as end_date,
        '00:00:00' as end_time
    FROM ovst o
    inner JOIN iptoprt i on i.an = o.an
    INNER JOIN icd9cm1 c on c.code = i.icd9
    WHERE o.vn = '${seq}'
    GROUP BY o.vn,i.icd9   
    `);
    return data[0];
  }
  async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
   select o.vn as seq,o.hn,
           DATE_FORMAT(date(o.vstdate),'%Y-%m-%d') as date_serv,
           DATE_FORMAT(time(o.vsttime),'%h:%i:%s') as time_serv,
           p.bloodgrp as bloodgrp,
           s.bw as weight,
           s.height as height,
           s.bmi as bmi,
           s.temperature as temperature,
           s.pulse as pr,
           s.rr as rr,
           s.bps as sbp,
           s.bpd as dbp,
           REPLACE(s.cc,"\r\n"," ") as symptom,
           o.main_dep as depcode,
           k.department as department,
           n.gcs_m as movement_score,n.gcs_v as vocal_score,n.gcs_e as eye_score,n.o2sat as oxygen_sat,(n.gcs_e+n.gcs_v+n.gcs_m) as gak_coma_sco,
           if((ovstdiag_text.provisional_dx_text="" or ovstdiag_text.provisional_dx_text is null),(select ro.pre_diagnosis from referout ro where ro.vn=o.vn and o.vn ='${seq}' limit 1) ,ovstdiag_text.provisional_dx_text) as diag_text,
      '' as pupil_right, '' as pupil_left,REPLACE(ro.pmh,"\r\n"," ") as pmh
    FROM ovst as o
    LEFT JOIN opdscreen as s on o.vn = s.vn
    INNER JOIN kskdepartment k ON k.depcode = o.main_dep
    INNER JOIN patient as p ON p.hn = o.hn
    LEFT JOIN referout as ro on ro.vn=o.vn
    LEFT JOIN  er_nursing_detail as n ON o.vn=n.vn
    left outer join ovstdiag_text on ovstdiag_text.vn=o.vn
    WHERE o.vn  = '${seq}'
    UNION
    select o.vn as seq,o.hn,
           DATE_FORMAT(date(o.regdate),'%Y-%m-%d') as date_serv,
           DATE_FORMAT(time(o.regtime),'%h:%i:%s') as time_serv,
           p.bloodgrp as bloodgrp,
           s.bw as weight,
           s.height as height,
           s.bmi as bmi,
           s.temperature as temperature,
           s.pulse as pr,
           s.rr as rr,
           s.bps as sbp,
           s.bpd as dbp,
           replace(replace(s.cc,char(13),' '),char(10),' ') as symptom,
           ""as depcode,
          "" as department,
           n.gcs_m as movement_score,n.gcs_v as vocal_score,n.gcs_e as eye_score,n.o2sat as oxygen_sat,(n.gcs_e+n.gcs_v+n.gcs_m) as gak_coma_sco,
           if((ovstdiag_text.provisional_dx_text="" or ovstdiag_text.provisional_dx_text is null),(select ro.pre_diagnosis from referout ro where ro.vn=o.vn and o.an ='${seq}' limit 1) ,ovstdiag_text.provisional_dx_text) as diag_text,
          '' as pupil_right, '' as pupil_left,REPLACE(ro.pmh,"\r\n"," ") as pmh
    FROM ipt as o
    LEFT JOIN opdscreen as s on o.vn = s.vn
    INNER JOIN patient as p ON p.hn = o.hn
    LEFT JOIN referout as ro on ro.vn=o.an
    LEFT JOIN  er_nursing_detail as n ON o.vn=n.vn
    left outer join ovstdiag_text on ovstdiag_text.vn=o.vn
    WHERE o.an  ='${seq}'
    `);
    return data[0];
  }
  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
        SELECT v.seq,GROUP_CONCAT(v.pe) as pe
        from(
        SELECT o.vn as seq, REPLACE(o.pe,"\r\n"," ") as pe FROM opdscreen o WHERE o.vn = '${seq}'
        union all
        select i.vn as seq,REPLACE(d.note,"\r\n"," ") as pe from ipt_discharge_note d
        left outer join ipt i on i.an=d.an
        where i.vn='${seq}'
        ) v
        group by seq
        `);
    return data[0];
  }
  async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
            select x.seq, REPLACE(GROUP_CONCAT(DISTINCT x.hpi),","," ") as hpi from 
            (
             SELECT o.vn as seq , REPLACE(o.hpi,"\r\n"," ") as hpi 
              FROM referout as o WHERE o.vn = '${seq}'
             union
             SELECT o.vn as seq , REPLACE(o.hpi,"\r\n"," ") as hpi 
                FROM opdscreen as o WHERE o.vn = '${seq}'
             ) as x
    `);
    return data[0];
  }
  async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT x.request_date as xray_date ,i.xray_items_name  as xray_name
    FROM xray_report x
    left outer join xray_items i on i.xray_items_code = x.xray_items_code  
    WHERE x.confirm = 'Y'  and x.confirm_read_film <> 'Y' and x.vn = '${seq}' 
    UNION
    SELECT x.request_date as xray_date ,i.xray_items_name  as xray_name
    FROM xray_report x
    left outer join xray_items i on i.xray_items_code = x.xray_items_code  
    WHERE x.confirm = 'Y'  and x.confirm_read_film <> 'Y' and x.an = '${seq}' 
    GROUP BY x.xn
    `);
    return data[0];
  }
}
