import { AxiosInstance } from "axios";
import  axios  from "axios";

export class PharmacyModels {
          
    private axiosInstance = axios.create({
        baseURL: `https://drug-api.sunpasit.go.th/api/drugallergy`
    })

  getImportPharmacy( token: any, datas: any) {
    const url: any = '/service/get-adr';
    return this.axiosInstance.post(url,datas ,{
      headers: {
        'Authorization': 'Bearer ' + token,
      }
    });
  }

}