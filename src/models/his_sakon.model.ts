import { Knex } from 'knex';
export class HisSakonModel {
    async getLogin(db: Knex, username: any, password: any) {
        let data = await db.raw(`
        SELECT username , fullname, hcode
        from smartrefer_user WHERE username = '${username}' and password = '${password}'
        `);
        return data;
    }
    async getVisit(db: Knex, startDate: any, endDtae: any) {
        let data = await db.raw(`
        SELECT 
        seq,hn, title_name, first_name, last_name, 
		date_serv, time_serv, department
		from smartrefer_visit
        WHERE date_serv between'${startDate}' and '${endDtae}'
        `);
        return data;
    }
    async getServices(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        SELECT 
        seq,hn, title_name, first_name, last_name, 
		date_serv, time_serv, department
		from smartrefer_visit
        WHERE hn = '${hn}' and seq = '${seq}'`);
        return data;
    }
    async getProfile(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        select hn, cid, title_name,first_name, last_name,
        moopart, addrpart, tmbpart, amppart, chwpart, brthdate
        , age, pttype_id, pttype_name, pttype_no, hospmain
        , hospmain_name, hospsub, hospsub_name, registdate, visitdate
        , father_name, mother_name, couple_name, contact_name,contact_relation, contact_mobile
        FROM smartrefer_person
        WHERE hn ='${hn}'`);
        return data;
    }
    async getHospital(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT provider_code, provider_name 
        FROM smartrefer_hospital`);
        return data;
    }
    async getAllergyDetail(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT drug_name, symptom ,begin_date,daterecord
        FROM smartrefer_allergy 
        WHERE hn ='${hn}'`);
        return data;
    }
    async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT seq, date_serv, time_serv
        , icd_code, cd_name
        , diag_type, DiagNote, diagtype_id
        FROM smartrefer_diagnosis  
        WHERE seq ='${seq}'`);
        return data;
    }
    async getDrugs(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        select seq, date_serv, time_serv, drug_name,
        qty, unit, usage_line1, usage_line2, usage_line3
        FROM smartrefer_drugs
        WHERE seq = '${seq}'`);
        return data;
    }
    async getLabs(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT date_serv, time_serv, labgroup, lab_name, lab_result, unit, standard_result
        FROM smartrefer_lab where seq ='${seq}'`);
        return data;
    }
    async getProcedure(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT pid, seq, date_serv,	time_serv, procedure_code,	
        procedure_name, start_date, start_time, end_date, end_time
        from smartrefer_procedure
        where seq = '${seq}'
        `);
        return data;
    }
    async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        select seq,date_serv,time_serv,bloodgrp,weight,height,bmi,temperature,pr,rr,sbp,dbp,symptom,depcode,department,movement_score,vocal_score,eye_score,oxygen_sat,gak_coma_sco,diag_text,pupil_right,pupil_left
        FROM smartrefer_nurtures
        WHERE seq = '${seq}'
        `);
        return data;
    }
    async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT seq , pe FROM smartrefer_physical WHERE seq = '${seq}'
        `);
        return data;
    }
    async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT seq , hpi FROM smartrefer_pillness WHERE seq = '${seq}'
        `);
        return data;
    }
    async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT xray_date ,xray_name 
        FROM smartrefer_xray 
        WHERE seq = '${seq}'
        `);
        return data;
    }
}
